import React from "react";

export default function PhoneItem({ index, handleLayThongTinSanPham }) {
  return (
    <div className="col-4 container">
      <div>
        <img style={{ height: "400px", width: "400px" }} src={index.hinhAnh} />
        <div>
          <h2>{index.tenSP}</h2>
          <p class="text-warning font-weight-bold" style={{ fontSize: "30px" }}>
            {index.giaBan} VNĐ
          </p>
          <button
            onClick={() => {
              handleLayThongTinSanPham(index.maSP);
            }}
            className="btn btn-success"
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
}
