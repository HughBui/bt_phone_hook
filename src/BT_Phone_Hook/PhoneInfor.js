import React from "react";

export default function PhoneInfor({ phoneInfor }) {
  console.log(phoneInfor.hinhAnh);
  return (
    <div className="row mt-5 ml-5">
      <div className="col-4 d-flex align-items-center">
        <div>
          <h2>{phoneInfor.tenSP}</h2>
          <img
            className="mt5"
            src={phoneInfor.hinhAnh}
            style={{ width: "400px", height: "400px" }}
          />
        </div>
      </div>
      <div className="col-8 mt-5">
        <h3 className="mb-5">Thông số kỹ thuật</h3>
        <table className="table">
          <tbody>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold">Màn hình</td>
              <td className="col-8 text-left">{phoneInfor.manHinh}</td>
            </tr>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold ">
                Hệ điều hành
              </td>
              <td className="col-8 text-left">{phoneInfor.heDieuHanh}</td>
            </tr>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold">Camera trước</td>
              <td className="col-8 text-left">{phoneInfor.cameraTruoc}</td>
            </tr>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold">Camera sau</td>
              <td className="col-8 text-left">{phoneInfor.cameraSau}</td>
            </tr>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold">Ram</td>
              <td className="col-8 text-left">{phoneInfor.ram}</td>
            </tr>
            <tr className="row">
              <td className="col-4 text-left font-weight-bold">Rom</td>
              <td className="col-8 text-left">{phoneInfor.rom}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}
