import React, { useCallback, useState } from "react";
import { dataPhone } from "./data";
import PhoneInfor from "./PhoneInfor";
import PhoneList from "./PhoneList";

export default function () {
  const [data, setData] = useState(dataPhone);
  const [phoneInfor, setPhoneInfor] = useState({ ...data[""] });
  const [visibility, setVisibility] = useState("hidden");
  const handleLayThongTinSanPham = useCallback(
    (maSP) => {
      let cloneData = [...data];
      let index = cloneData.findIndex((item) => {
        return item.maSP == maSP;
      });
      let phone = cloneData[index];
      setVisibility("");
      setPhoneInfor({ ...phone });
    },
    [phoneInfor]
  );
  return (
    <div>
      <div className="font-weight-bold display-4 m-5 text-secondary">
        Cửa Hàng Điện Thoại HOOK
      </div>
      <div>
        <PhoneList
          handleLayThongTinSanPham={handleLayThongTinSanPham}
          data={data}
        />
      </div>
      <div style={{ visibility: visibility }}>
        <PhoneInfor phoneInfor={phoneInfor} />
      </div>
    </div>
  );
}
