import React from "react";
import PhoneItem from "./PhoneItem";

export default function PhoneList({ handleLayThongTinSanPham, data }) {
  return (
    <div>
      <div className="row">
        {data.map((item) => {
          return (
            <PhoneItem
              key={item.maSP}
              index={item}
              handleLayThongTinSanPham={handleLayThongTinSanPham}
            />
          );
        })}
      </div>
    </div>
  );
}
